package indiamart.scraper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.webharvest.definition.ScraperConfiguration;
import org.webharvest.runtime.Scraper;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class CompanyScraper extends BaseScraper implements Callable<List<CompanyObject>>{
	private static DocumentBuilder builder = null;
	private static XPathFactory factory = XPathFactory.newInstance();
	static{
		try{
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch(Exception e){
			System.out.println("Unable to initiate docuemnt builder. Exiting!");
			System.exit(-1);
		}
	}
	
	private String configContent;
	private String URL;
	private static AtomicBoolean stopFutureMonitoring = new AtomicBoolean(false);
	private static final Log logger = LogFactory.getLog(CompanyScraper.class);
	public CompanyScraper(String config, String URL){
		this.configContent = config;
		this.URL = URL;
		isProxyEnabled = false;
	}
	
	public CompanyScraper(String proxyHost, int port, String userName, String password, String ntlmDomain, String ntlmHost, String config, String URL) {
		super(proxyHost, port, userName, password, ntlmDomain, ntlmHost);
		this.configContent = config;
		this.URL = URL;
		isProxyEnabled = true;
	}



	public List<CompanyObject> scrapeWebpageForCompany() throws Exception{
		logger.info("Scrape request arrived for: "+URL);
		ScraperConfiguration config = new ScraperConfiguration(new InputSource(
				new StringReader(configContent.replaceAll("#URL#", URL))));
		Scraper scraper = new Scraper(config,
				System.getProperty("java.io.tmpdir"));
		handleProxy(scraper);
		logger.info("Scrapping execution will start!");
		scraper.execute();
		logger.info("Scrapping execution Ended! Start extracting the variables.");
		Object catalog = scraper.getContext().getVar(ScraperVariables.companyCatalog.getValue());
		String nexURL = scraper.getContext().getVar(ScraperVariables.next.getValue()).toString();
		ScraperURLMaintainer.push(nexURL);
		List<CompanyObject> companies = unmarshallXMLDocumentToCreateObject(catalog.toString());
		//System.out.println(scraper.getContext().getVar("webpage").toString());
		logger.info("All variables are extracted. Constructing restaurant object.");
		return companies;
	}
	private List<CompanyObject> unmarshallXMLDocumentToCreateObject(
			String documentStr) throws SAXException, IOException, XPathExpressionException {
		Document doc = builder.parse(new InputSource(new StringReader(documentStr.replaceAll("\\n", ""))));
		NodeList nodeList = doc.getFirstChild().getChildNodes();
		List<CompanyObject> retList = new ArrayList<CompanyObject>();
		for(int i=0;i<nodeList.getLength();i++){
			Node product = nodeList.item(i);
			XPath xpath = factory.newXPath();
			String name = xpath.evaluate("name/text()", product);
			String address = xpath.evaluate("address/text()", product);
			String phone = xpath.evaluate("phone/text()", product);
			String phone_1 = xpath.evaluate("phone_1/text()", product);
			String website = xpath.evaluate("website/text()", product);
			retList.add(new CompanyObject(name, address, phone.trim().isEmpty()?phone_1:phone, website));
		}

		return retList;
	}

	public List<CompanyObject> call() throws Exception {
		List<CompanyObject> obj = null;
		try{
			obj = scrapeWebpageForCompany();
		} catch(Exception e){
			logger.error("Unable to scrape this restaurant details. [URL: "+URL+"]", e);
		}
		return obj;
	}

	public static void main(String args[]) throws Exception{
		String startURL = "sares.html";
		ScraperURLMaintainer.push(startURL);
		String scraperFileName = "indiamart-config.xml";
		final String configContent = CompanyScraper.readFile(scraperFileName);
		final Executor e = new ThreadPoolExecutor(50, 100, Long.MAX_VALUE, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>());
		final CompletionService<List<CompanyObject>> cs = new ExecutorCompletionService<List<CompanyObject>>(e);
		new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					startMonitoring(cs, configContent);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
		new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					writeFile(cs);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public static void writeFile(CompletionService<List<CompanyObject>> cs) throws Exception{
		List<CompanyObject> allCompanies = new ArrayList<CompanyObject>();
		while(!stopFutureMonitoring.get()){
			Future<List<CompanyObject>> companies = cs.poll();
			if(companies!=null){
				List<CompanyObject> companyObjs = companies.get();
				if(companyObjs!=null && !companyObjs.isEmpty()){
					/*System.out.println("Writing "+companyObjs.size()+" company records.");
					for(CompanyObject company : companyObjs)
						allCompanies.add(company.toString());*/
					allCompanies.addAll(companyObjs);
				}
			}
			Thread.sleep(1000);
		}
/*		FileOutputStream fos = new FileOutputStream(new File("C:/Debal/catalog-indiamart-csv.csv"));
		Writer writer = new OutputStreamWriter(fos);*/
		//FileUtils.writeLines(new File("C:/Debal/catalog-indiamart-csv.csv"), allCompanies);
//		for(String line : allCompanies)
//			System.out.println(line);
		writeExcelFie(allCompanies);
		System.exit(1);
	}
	
	private static void startMonitoring(CompletionService<List<CompanyObject>> cs, String configContent) throws InterruptedException{
		int retry = 0;
		int retryLimit = 5;
		int urlCount = 0;
		while(true){
			if(retry == retryLimit || urlCount ==20)
				break;
			String nextURL = ScraperURLMaintainer.getNextURLToVisit();
			if(nextURL != null && !nextURL.trim().isEmpty()){
				cs.submit(new CompanyScraper(configContent, "http://dir.indiamart.com/indianexporters/".concat(nextURL)));
				urlCount++;
				retry=0;
			}else{
				//System.out.println("No next page to scrape!");
				retry++;
			}
			Thread.sleep(2000);
		}
		stopFutureMonitoring.compareAndSet(false, true);
	}
	
	private static String readFile(String fileName) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(CompanyScraper.class.getClassLoader().getResourceAsStream(fileName)));
		String line = "";
		StringBuilder builder = new StringBuilder();
		while ((line = reader.readLine()) != null)
			builder.append(line);
		return builder.toString();
	}	
	
	private static void writeExcelFie(Collection<CompanyObject> companies) throws Exception {
		int sheetThreshold = 30000;
		int sheetCounter = 1;
		File xlFile = new File("C:/Debal/indiamart-scrape.xls");
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet sheet = workBook.createSheet("Sheet - " + (sheetCounter++));
		int cellnum = 0;
		int rownum = 0;
		Row row = sheet.createRow(rownum++);
		Cell nameCell = row.createCell(cellnum++);
		Cell phoneCell = row.createCell(cellnum++);
		Cell addressCell = row.createCell(cellnum++);
		Cell websiteCell = row.createCell(cellnum++);
		nameCell.setCellValue("Name");
		phoneCell.setCellValue("Phone");
		addressCell.setCellValue("Address");
		websiteCell.setCellValue("Website");
		for (CompanyObject company : companies) {
			if (rownum == sheetThreshold) {
				rownum = 0;
				sheet = workBook.createSheet("Sheet - " + (sheetCounter++));
			}
			row = sheet.createRow(rownum++);
			cellnum = 0;
			nameCell = row.createCell(cellnum++);
			phoneCell = row.createCell(cellnum++);
			addressCell = row.createCell(cellnum++);
			websiteCell = row.createCell(cellnum++);			

			nameCell.setCellValue(company.getCompanyName());
			phoneCell.setCellValue(company.getCompanyPhone());

			addressCell.setCellValue(company.getCompanyAddress());
			websiteCell.setCellValue(company.getWebsite());
		}
		FileOutputStream outs = new FileOutputStream(xlFile);
		workBook.write(outs);
		outs.close();
	}	
}
