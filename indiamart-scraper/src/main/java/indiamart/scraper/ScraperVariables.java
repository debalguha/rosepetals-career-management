package indiamart.scraper;

public enum ScraperVariables {
	name("name", VariableType.SINGLE), streetAddress("streetAddress", VariableType.SINGLE), city("city", VariableType.SINGLE), 
	region("region", VariableType.SINGLE), neighbourhood("neighbourhood", VariableType.SINGLE), phone("phone", VariableType.SINGLE), 
	price("name", VariableType.SINGLE), hours("hours", VariableType.LIST), cuisines("cuisines", VariableType.LIST), 
	services("services", VariableType.LIST), meals("meals", VariableType.LIST), features("features", VariableType.LIST), companyCatalog("companyCatalog", VariableType.SINGLE),
	next("next", VariableType.SINGLE);
	private String value;
	private VariableType variableType;
	ScraperVariables(String value, VariableType type){
		this.value = value;
		this.variableType = type;
	}
	public String getValue(){
		return this.value;
	}
	public VariableType getVariableType(){
		return this.variableType;
	}
}
