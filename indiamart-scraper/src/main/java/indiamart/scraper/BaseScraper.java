package indiamart.scraper;

import org.webharvest.runtime.Scraper;
import org.webharvest.runtime.web.HttpClientManager;

public abstract class BaseScraper {

	protected boolean isProxyEnabled;
	protected String proxyHost;
	protected int port;
	protected String userName;
	protected String password;
	protected String ntlmDomain;
	protected String ntlmHost;
	protected String paramSuffix;

	public BaseScraper(String proxyHost, int port, String userName, String password, String ntlmDomain, String ntlmHost) {
		super();
		this.proxyHost = proxyHost;
		this.port = port;
		this.userName = userName;
		this.password = password;
		this.ntlmDomain = ntlmDomain;
		this.ntlmHost = ntlmHost;
	}

	public BaseScraper() {
		super();
	}

	protected void handleProxy(Scraper scraper) {
		if(isProxyEnabled){
			HttpClientManager manager = scraper.getHttpClientManager();
			manager.setHttpProxy(this.proxyHost, this.port);
			manager.setHttpProxyCredentials(this.userName, this.password, this.ntlmHost, this.ntlmDomain);
		}
	}
}