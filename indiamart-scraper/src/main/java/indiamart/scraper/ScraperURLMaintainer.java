package indiamart.scraper;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ScraperURLMaintainer {
	private static Queue<String> toVisit = new LinkedBlockingQueue<String>();
	public static synchronized void push(String url){
		System.out.println("Adding UR: "+url);
		toVisit.offer(url);
	}
	public static synchronized String getNextURLToVisit(){
		return toVisit.poll();
	}
}
