package indiamart.scraper;


public class CompanyObject implements Comparable<CompanyObject>{
	private String companyName;
	private String companyAddress;
	private String companyPhone;
	private String website;
	public CompanyObject(String companyName, String companyAddress,
			String companyPhone, String website) {
		super();
		this.companyName = companyName;
		this.companyAddress = companyAddress;
		this.companyPhone = companyPhone;
		this.website = website;
	}
	@Override
	public int compareTo(CompanyObject arg0) {
		return this.companyName.compareTo(arg0.companyName);
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	@Override
	public String toString() {
		return companyName
				+ ", " + companyAddress + ", "
				+ companyPhone + ", " + website + "]";
	}
	public String getWebsite() {
		return website;
	}
	
}
