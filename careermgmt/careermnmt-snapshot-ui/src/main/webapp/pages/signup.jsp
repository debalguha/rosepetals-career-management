<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Registration</title>

<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-responsive.css">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ui-lightness/jquery-ui-1.8.21.custom.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/application.css">

<script src="${pageContext.request.contextPath}/js/libs/modernizr-2.5.3.min.js"></script>

</head>

<body class="login">




	<div class="account-container register stacked">

		<div class="content clearfix">

			<form action="registration/register.do" method="post">

				<h1>Create Your Account</h1>

				<div class="login-social">
					<p>Sign in using social network:</p>

					<div class="twitter">
						<a href="#" class="btn_1">Login with Twitter</a>
					</div>

					<div class="fb">
						<a href="#" class="btn_2">Login with Facebook</a>
					</div>
				</div>

				<div class="login-fields">

					<p>Create your free account:</p>

					<div class="field">
						<label for="firstname">First Name:</label> <input type="text"
							id="userName" name="userName" value="" placeholder="User Name"
							class="login" />
					</div>
					<!-- /field -->

					<div class="field">
						<label for="email">Email Address:</label> <input type="text"
							id="email" name="email" value="" placeholder="Email"
							class="login" />
					</div>
					<!-- /field -->

					<div class="field">
						<label for="password">Password:</label> <input type="password"
							id="password" name="password" value="" placeholder="Password"
							class="login" />
					</div>
					<!-- /field -->

					<div class="field">
						<label for="confirm_password">Confirm Password:</label> <input
							type="password" id="confirm_password" name="confirm_password"
							value="" placeholder="Confirm Password" class="login" />
					</div>
					<!-- /field -->

				</div>
				<!-- /login-fields -->

				<div class="login-actions">

					<span class="login-checkbox"> <input id="Field" name="Field"
						type="checkbox" class="field login-checkbox" value="First Choice"
						tabindex="4" /> <label class="choice" for="Field">I have
							read and agree with the Terms of Use.</label>
					</span>

					<button class="button btn btn-primary btn-large">Register</button>

				</div>
				<!-- .actions -->

			</form>

		</div>
		<!-- /content -->

	</div>
	<!-- /account-container -->


	<!-- Text Under Box -->
	<div class="login-extra">
		Already have an account? <a href="./login.html">Login</a>
	</div>
	<!-- /login-extra -->




	<script src="${pageContext.request.contextPath}/js/libs/jquery-1.7.2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.ui.touch-punch.min.js"></script>

	<script src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>

	<script src="${pageContext.request.contextPath}/js/signin.js"></script>

</body>
</html>
