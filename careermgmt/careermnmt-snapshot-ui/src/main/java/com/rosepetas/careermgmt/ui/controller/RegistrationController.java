package com.rosepetas.careermgmt.ui.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.rosepetals.careermgmt.model.User;
import com.rosepetals.careermgmt.service.UserService;

@Controller
@RequestMapping({ "/registration" })
public class RegistrationController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/register.do", method = RequestMethod.POST)
	public @ResponseBody
	String doRegister(@ModelAttribute("newMember") User user, BindingResult result, Model model, HttpServletRequest request){
		return null;
	}
	
	@RequestMapping(value = "/openRegistration.do", method = RequestMethod.GET)
	public ModelAndView doRegister(){
		return new ModelAndView("signup");
	}	

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
