package com.rosepetas.careermgmt.ui.model;

import java.util.Date;

public class RegistrationVO {
	private String firstName;
	private String lastName;
	private String address;
	private String email;
	private int countryCode;
	private int areaCode;
	private long phoneNum;
	private Date birthDay;
	private String password;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public int getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}
	public long getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(long phoneNum) {
		this.phoneNum = phoneNum;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "RegistrationModel [firstName=" + firstName + ", lastName="
				+ lastName + ", address=" + address + ", email=" + email
				+ ", countryCode=" + countryCode + ", areaCode=" + areaCode
				+ ", phoneNum=" + phoneNum + ", birthDay=" + birthDay
				+ ", password=" + password + "]";
	}
}
