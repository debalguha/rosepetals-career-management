package com.rosepetals.careermgmt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sticky extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	@Column(nullable=false, length=4096)
	private String stickyNote;
	@Column(nullable=false)
	private StickyType stickyType;
	
	private Date creationDate;
	private Date updateDate;
	private boolean active;
	
	public String getStickyNote() {
		return stickyNote;
	}

	public void setStickyNote(String stickyNote) {
		this.stickyNote = stickyNote;
	}

	public StickyType getStickyType() {
		return stickyType;
	}

	public void setStickyType(StickyType stickyType) {
		this.stickyType = stickyType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

/*	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}*/


/*	public CareerStage getCareerStage() {
		return careerStage;
	}

	public void setCareerStage(CareerStage careerStage) {
		this.careerStage = careerStage;
	}*/

/*	public StickyPK getPk() {
		return pk;
	}

	public void setPk(StickyPK pk) {
		this.pk = pk;
	}*/
	
	@Override
	public long getID() {
		//throw new RuntimeException("Composite ID. Can not return a long.");
		return ID;
	}	
}
