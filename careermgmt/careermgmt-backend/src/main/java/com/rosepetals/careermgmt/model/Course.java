package com.rosepetals.careermgmt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(
	name = "findCourseByName",
	query = "from Course s where s.courseName = :courseName"
	)
})
public class Course extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	@Column(nullable=false, length=100)
	private String courseName;
	private String minAge;
	@Column(nullable=false, length=100)
	private Standard courseStandard;
	private int durationInMonths;
	
	private Date creationDate;
	private Date updateDate;
	
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getMinAge() {
		return minAge;
	}
	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}
	public Standard getCourseStandard() {
		return courseStandard;
	}
	public void setCourseStandard(Standard courseStandard) {
		this.courseStandard = courseStandard;
	}
	public int getDurationInMonths() {
		return durationInMonths;
	}
	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}
	@Override
	public String toString() {
		return "Course [courseName=" + courseName + ", minAge=" + minAge + ", courseStandard=" + courseStandard + ", durationInMonths=" + durationInMonths + "]";
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	@Override
	public long getID() {
		return ID;
	}
	
}
