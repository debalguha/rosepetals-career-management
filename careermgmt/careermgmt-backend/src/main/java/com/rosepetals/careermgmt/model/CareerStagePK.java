package com.rosepetals.careermgmt.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class CareerStagePK implements Serializable{
	@ManyToOne
	private Student student;

	@ManyToOne
	private Course course;

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		CareerStagePK that = (CareerStagePK) obj;
		if (student != null ? !student.equals(that.student) : that.student != null)
			return false;
		if (course != null ? !course.equals(that.course) : that.course != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (student != null ? student.hashCode() : 0);
		result = 31 * result + (course != null ? course.hashCode() : 0);
		return result;
	}
}
