package com.rosepetals.careermgmt.persistence;

import java.util.Date;
import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.rosepetals.careermgmt.model.BaseModel;
import com.rosepetals.careermgmt.model.Course;
import com.rosepetals.careermgmt.model.CourseStandard;
import com.rosepetals.careermgmt.model.User;

@Repository("careerSnapshotsDAO")
public class CareermanagementDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public User getUserByUserName(String userName){
		Query query = entityManager.createNamedQuery("findUserById").setParameter("userName", userName);
		Iterator<User> itr = query.getResultList().iterator();
		while(itr.hasNext())
			return itr.next();
		throw new RuntimeException("No user found with given user name.");
	}
	
	@SuppressWarnings("unchecked")
	public Course getCourseByCourseName(String courseName){
		Query query = entityManager.createNamedQuery("findCourseByName").setParameter("courseName", courseName);
		Iterator<Course> itr = query.getResultList().iterator();
		while(itr.hasNext())
			return itr.next();
		throw new RuntimeException("No user found with given user name.");
	}	
	
	public CourseStandard getCourseStandardByID(long id){
		return entityManager.find(CourseStandard.class, new Long(id));
	}
	
	public void saveEntity(BaseModel model){
		Date nowDate = new Date();
		model.setUpdateDate(nowDate);
		if(model.getCreationDate()==null)
			model.setCreationDate(nowDate);
		entityManager.persist(model);
	}
	
	public void updateEntity(BaseModel model){
		Date nowDate = new Date();
		model.setUpdateDate(nowDate);
		entityManager.merge(model);
	}
	
	/*public void saveCourse(Course course){
		Date nowDate = new Date();
		course.setUpdateDate(nowDate);
		if(course.getCreationDate()==null)
			course.setCreationDate(nowDate);
		entityManager.persist(course);
	}
	
	public void saveUser(User user){
		
	}*/
}
