package com.rosepetals.careermgmt.model;

public enum Standard {
	_10TH("10th"), _12TH("12th"), GRAD("Graduate"), PGRAD("Post Graduate"), DOC("Post Doc"), PROFFESSIONAL("Professional");
	
	private String standardDesc;
	Standard(String standardDesc){
		this.standardDesc = standardDesc;
	}
	public String getStandardDesc() {
		return standardDesc;
	}
	@Override
	public String toString() {
		return getStandardDesc();
	}
}
