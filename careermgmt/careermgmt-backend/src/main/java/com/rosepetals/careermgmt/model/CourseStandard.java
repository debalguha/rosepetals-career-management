package com.rosepetals.careermgmt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CourseStandard extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	private Standard standard;
	
	private Date creationDate;
	private Date updateDate;
	
	public Standard getStandard() {
		return standard;
	}
	public void setStandard(Standard standard) {
		this.standard = standard;
	}
	public long getID() {
		return ID;
	}
	@Override
	public Date getCreationDate() {
		return creationDate;
	}
	@Override
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
