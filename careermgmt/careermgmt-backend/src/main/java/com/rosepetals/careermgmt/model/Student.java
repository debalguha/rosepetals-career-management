package com.rosepetals.careermgmt.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Student extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	@Column(nullable=false, length=50)
	private String firstName;
	@Column(nullable=false, length=50)	
	private String lastName;
	@Column(nullable=false, length=1024)	
	private String address;
	private int countryCode;
	private int areaCode;
	private long phoneNum;
	private Date birthDay;
	private Date creationDate;
	private Date updateDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="pk.student")
	private Set<CareerStage> careerStages;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="student")
	private Set<StudentMission> missionStatements;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="student")
	private Set<SelfEvaluation> selfEvaluations;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="student")
	private Set<Endorsement> endorsements;
	
	/*@OneToMany(fetch = FetchType.LAZY, mappedBy="student")
	private Set<Sticky> stickies;	*/
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public int getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}
	public long getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(long phoneNum) {
		this.phoneNum = phoneNum;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public String toString() {
		return "Student [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", countryCode=" + countryCode + ", areaCode=" + areaCode + ", phoneNum=" + phoneNum + ", birthDay=" + birthDay + ", creationDate=" + creationDate + "]";
	}
	public Set<CareerStage> getCareerStages() {
		return careerStages;
	}
	public void setCareerStages(Set<CareerStage> careerStages) {
		this.careerStages = careerStages;
	}
	public Set<StudentMission> getMissionStatements() {
		return missionStatements;
	}
	public void setMissionStatements(Set<StudentMission> missionStatements) {
		this.missionStatements = missionStatements;
	}
	public Set<SelfEvaluation> getSelfEvaluations() {
		return selfEvaluations;
	}
	public void setSelfEvaluations(Set<SelfEvaluation> selfEvaluations) {
		this.selfEvaluations = selfEvaluations;
	}
	public Set<Endorsement> getEndorsements() {
		return endorsements;
	}
	public void setEndorsements(Set<Endorsement> endorsements) {
		this.endorsements = endorsements;
	}
/*	public Set<Sticky> getStickies() {
		return stickies;
	}
	public void setStickies(Set<Sticky> stickies) {
		this.stickies = stickies;
	}*/
	public long getID() {
		return ID;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
