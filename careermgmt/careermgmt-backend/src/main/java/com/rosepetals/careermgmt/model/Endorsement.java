package com.rosepetals.careermgmt.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Endorsement extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	private String endorsementStatement;
	private String endorserName;
	private String endorserSalutation;
	
	private Date creationDate;
	private Date updateDate;
	
	private boolean active;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	@JoinColumn(name="student_id")
	private Student student;

	public String getEndorsementStatement() {
		return endorsementStatement;
	}

	public void setEndorsementStatement(String endorsementStatement) {
		this.endorsementStatement = endorsementStatement;
	}

	public String getEndorserName() {
		return endorserName;
	}

	public void setEndorserName(String endorserName) {
		this.endorserName = endorserName;
	}

	public String getEndorserSalutation() {
		return endorserSalutation;
	}

	public void setEndorserSalutation(String endorserSalutation) {
		this.endorserSalutation = endorserSalutation;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public long getID() {
		return ID;
	}
}
