package com.rosepetals.careermgmt.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

@Entity
@AssociationOverrides({ @AssociationOverride(name = "pk.student", joinColumns = @JoinColumn(name = "student_id")), @AssociationOverride(name = "pk.course", joinColumns = @JoinColumn(name = "course_id")) })
public class CareerStage extends BaseModel{

	@EmbeddedId
	private CareerStagePK pk = new CareerStagePK();

	private Date creationDate;
	private Date updateDate;
	private Date courseEndDate;
	private Date courseStartDate;

	private boolean active;
	
	@ManyToMany
	private Set<Sticky> stickies;

	@Transient
	public Student getStudent() {
		return pk.getStudent();
	}

	public void setStudent(Student student) {
		pk.setStudent(student);
	}

	@Transient
	public Course getCourse() {
		return pk.getCourse();
	}

	public void setCourse(Course course) {
		pk.setCourse(course);
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CareerStage that = (CareerStage) o;

		if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (getPk() != null ? getPk().hashCode() : 0);
	}

	public CareerStagePK getPk() {
		return pk;
	}

	public void setPk(CareerStagePK pk) {
		this.pk = pk;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCourseEndDate() {
		return courseEndDate;
	}

	public void setCourseEndDate(Date courseEndDate) {
		this.courseEndDate = courseEndDate;
	}

	public Date getCourseStartDate() {
		return courseStartDate;
	}

	public void setCourseStartDate(Date courseStartDate) {
		this.courseStartDate = courseStartDate;
	}

	@Override
	public long getID() {
		throw new RuntimeException("Composite ID. Can not return a long.");
	}

	public Set<Sticky> getStickies() {
		return stickies;
	}

	public void setStickies(Set<Sticky> stickies) {
		this.stickies = stickies;
	}
}
